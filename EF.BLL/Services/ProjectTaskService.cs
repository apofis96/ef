﻿using AutoMapper;
using EF.Common.DTO.ProjectTask;
using EF.Common.DTO.QueryResults;
using EF.BLL.Interfaces;
using EF.DAL.Interfaces;
using EF.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using EF.Common;

namespace EF.BLL.Services
{
    public class ProjectTaskService : IProjectTaskService
    {
        private readonly IRepository<ProjectTask> _projectTaskRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IMapper _mapper;
        public ProjectTaskService(IRepository<ProjectTask> projectTaskRepository, IRepository<User> userRepository, IMapper mapper)
        {
            _projectTaskRepository = projectTaskRepository;
            _userRepository = userRepository;
            _mapper = mapper;
        }
        public IEnumerable<ProjectTaskDTO> GetAll()
        {
            var tasks = _projectTaskRepository.Get();
            tasks = tasks.GroupJoin(_userRepository.Get(),
                t => t.PerformerId, u => u.Id, (t, u) =>
                 {
                     t.Performer = u.FirstOrDefault();
                     return t;
                 });
            return _mapper.Map<IEnumerable<ProjectTaskDTO>>(tasks);
        }
        public ProjectTaskDTO Get(int id)
        {
            ProjectTaskIsExistOrExeption(id);
            var task = _projectTaskRepository.Get(IRepository<ProjectTask>.FilterById(id)).FirstOrDefault();
            task.Performer = _userRepository.Get(IRepository<User>.FilterById(task.PerformerId)).FirstOrDefault();
            return _mapper.Map<ProjectTaskDTO>(task);
        }
        public ProjectTaskDTO Post(ProjectTaskCreateDTO task)
        {
            var newTask = _mapper.Map<ProjectTask>(task);
            _projectTaskRepository.Create(newTask);
            return Get(newTask.Id);
        }
        public ProjectTaskDTO Update(ProjectTaskUpdateDTO updateTask)
        {
            ProjectTaskIsExistOrExeption(updateTask.Id);
            var task = _projectTaskRepository.Get(IRepository<ProjectTask>.FilterById(updateTask.Id)).FirstOrDefault();
            task.Name = updateTask.Name;
            task.Description = updateTask.Description;
            task.FinishedAt = updateTask.FinishedAt;
            task.State = updateTask.State;
            task.ProjectId = updateTask.ProjectId;
            task.PerformerId = updateTask.PerformerId;
            _projectTaskRepository.Update(task);
            return Get(updateTask.Id);
        }
        public void Delete(int id)
        {
            ProjectTaskIsExistOrExeption(id);
            _projectTaskRepository.Delete(id);
        }
        public IEnumerable<ProjectTaskDTO> GetTasksByUser(int userId)
        {
            return GetAll().Where(t => t.Performer?.Id == userId && t.Name.Length < 45);
        }
        public IEnumerable<FinishedTaskDTO> GetFinishedTasks(int userId)
        {
            return _mapper.Map<IEnumerable<FinishedTaskDTO>>(GetAll().Where(t => t.Performer?.Id == userId && t.State == TaskState.Finished && t.FinishedAt.Year == 2020));
        }
        public void ProjectTaskIsExistOrExeption(int id)
        {
            if (_projectTaskRepository.Get(IRepository<ProjectTask>.FilterById(id)).FirstOrDefault() == null)
                throw new InvalidOperationException("Task not found");
        }

    }
}
