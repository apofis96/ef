﻿using EF.Common.DTO.Team;
using EF.Common.DTO.QueryResults;
using System.Collections.Generic;

namespace EF.BLL.Interfaces
{
    public interface ITeamService
    {
        IEnumerable<TeamDTO> GetAll();
        TeamDTO Get(int id);
        TeamDTO Post(TeamCreateDTO team);
        TeamDTO Update(TeamUpdateDTO team); 
        void Delete(int id);
        IEnumerable<TeamMembersDTO> GetTeamMembers();
        void TeamIsExistOrExeption(int id);
    }
}
