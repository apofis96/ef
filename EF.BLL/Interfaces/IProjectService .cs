﻿using EF.Common.DTO.Project;
using EF.Common.DTO.QueryResults;
using System.Collections.Generic;

namespace EF.BLL.Interfaces
{
    public interface IProjectService
    {
        IEnumerable<ProjectDTO> GetAll();
        ProjectDTO Get(int id);
        ProjectDTO Post(ProjectCreateDTO project);
        ProjectDTO Update(ProjectUpdateDTO project); 
        void Delete(int id);
        IEnumerable<TaskCountDTO> GetTasksCountByUser(int userId);
        UserStatisticDTO GetUserStatistics(int userId);
        IEnumerable<ProjectStatisticDTO> GetProjectStatistics();
        void ProjectIsExistOrExeption(int id);
    }
}
