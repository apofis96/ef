﻿using EF.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace EF.DAL.Models
{
    public class ProjectTask : Entity
    {
        [MaxLength(100)]
        public string Name { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskState State { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public User Performer { get; set; }
        public ProjectTask() : base() 
        {
            CreatedAt = DateTime.Now;
        }
    }
}
