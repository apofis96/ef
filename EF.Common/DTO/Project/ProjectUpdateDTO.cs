﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EF.Common.DTO.Project
{
    public class ProjectUpdateDTO
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [MinLength(2)]
        public string Name { get; set; }
        [Required]
        [MinLength(10)]
        public string Description { get; set; }
        [Required]
        public DateTime Deadline { get; set; }
        [Required]
        public int AuthorId { get; set; }
        [Required]
        public int TeamId { get; set; }
    }
}
