﻿using System.ComponentModel.DataAnnotations;

namespace EF.Common.DTO.Team
{
    public class TeamUpdateDTO
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [StringLength(100, MinimumLength = 2)]
        public string Name { get; set; }
        [StringLength(500, MinimumLength = 10)]
        public string Description { get; set; }
        
    }
}
