﻿using EF.Common.DTO.ProjectTask;
using EF.Common.DTO.Project;

namespace EF.Common.DTO.QueryResults
{
    public class ProjectStatisticDTO
    {
        public ProjectDTO Project { get; set; }
        public ProjectTaskDTO LongestDescriptionTask { get; set; }
        public ProjectTaskDTO ShortestNameTask { get; set; }
        public int MembersTeamCount { get; set; }
    }
}
