﻿using EF.Common.DTO.User;
using EF.Common.DTO.ProjectTask;
using System.Collections.Generic;

namespace EF.Common.DTO.QueryResults
{
    public class UserTasksDTO
    {
        public UserDTO User { get; set; }
        public IEnumerable<ProjectTaskDTO> Tasks { get; set; }

    }
}
