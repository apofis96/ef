﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using EF.Common.DTO.ProjectTask;
using EF.Common.DTO.QueryResults;
using EF.BLL.Interfaces;

namespace EF.WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TasksController : ControllerBase
    {
        private readonly IProjectTaskService _taskService;
        private readonly IProjectService _projectService;
        private readonly IUserService _userService;
        public TasksController(IProjectTaskService taskService, IProjectService projectService, IUserService userService)
        {
            _taskService = taskService;
            _projectService = projectService;
            _userService = userService;
        }
        [HttpGet]
        public IEnumerable<ProjectTaskDTO> Get()
        {
            return _taskService.GetAll();
        }
        [HttpGet("{id}", Name = "TaskGet")]
        public ActionResult<ProjectTaskDTO> Get(int id)
        {
            return Ok(_taskService.Get(id));
        }
        [HttpPost]
        public ActionResult<ProjectTaskDTO> Post([FromBody] ProjectTaskCreateDTO task)
        {
            _userService.UserIsExistOrExeption(task.PerformerId);
            _projectService.ProjectIsExistOrExeption(task.ProjectId);
            var newTask = _taskService.Post(task);
            return CreatedAtRoute("TaskGet", new { newTask.Id }, newTask);
        }
        [HttpPut]
        public ActionResult<ProjectTaskDTO> Put([FromBody] ProjectTaskUpdateDTO task)
        {
            _userService.UserIsExistOrExeption(task.PerformerId);
            _projectService.ProjectIsExistOrExeption(task.ProjectId);
            var updateTask = _taskService.Update(task);
            return CreatedAtRoute("TaskGet", new { updateTask.Id }, updateTask);
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _taskService.Delete(id);
            return NoContent();
        }
        [HttpGet("TasksByUser/{id}")]
        public ActionResult<IEnumerable<ProjectTaskDTO>> GetTasksCountByUser(int id)
        {
            _userService.UserIsExistOrExeption(id);
            return Ok(_taskService.GetTasksByUser(id));
        }
        [HttpGet("FinishedTasks/{id}")]
        public ActionResult<IEnumerable<FinishedTaskDTO>> GetFinishedTasks(int id)
        {
            _userService.UserIsExistOrExeption(id);
            return Ok(_taskService.GetFinishedTasks(id));
        }
    }
}
