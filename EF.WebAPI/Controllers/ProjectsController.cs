﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using EF.Common.DTO.Project;
using EF.Common.DTO.QueryResults;
using EF.BLL.Interfaces;

namespace EF.WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;
        private readonly IUserService _userService;
        private readonly ITeamService _teamService;
        public ProjectsController(IProjectService projectService, IUserService userService, ITeamService teamService)
        {
            _projectService = projectService;
            _userService = userService;
            _teamService = teamService;
        }
        [HttpGet]
        public IEnumerable<ProjectDTO> Get()
        {
            return _projectService.GetAll();
        }
        [HttpGet("{id}", Name = "ProjectGet")]
        public ActionResult<ProjectDTO> Get(int id)
        {
            return Ok(_projectService.Get(id));
        }
        [HttpPost]
        public ActionResult<ProjectDTO> Post([FromBody] ProjectCreateDTO project)
        {
            _userService.UserIsExistOrExeption(project.AuthorId);
            _teamService.TeamIsExistOrExeption(project.TeamId);
            var newProject = _projectService.Post(project);
            return CreatedAtRoute("ProjectGet", new { newProject.Id }, newProject);
        }
        [HttpPut]
        public ActionResult<ProjectDTO> Put([FromBody] ProjectUpdateDTO project)
        {
            _userService.UserIsExistOrExeption(project.AuthorId);
            _teamService.TeamIsExistOrExeption(project.TeamId);
            var updateProject = _projectService.Update(project);
            return CreatedAtRoute("ProjectGet", new { updateProject.Id }, updateProject);
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _projectService.Delete(id);
            return NoContent();
        }
        [HttpGet("TasksCountByUser/{id}")]
        public ActionResult<IEnumerable<TaskCountDTO>> GetTasksCountByUser(int id)
        {
            _userService.UserIsExistOrExeption(id);
            return Ok(_projectService.GetTasksCountByUser(id));
        }
        [HttpGet("UserStatistics/{id}")]
        public ActionResult<UserStatisticDTO> GetUserStatistics(int id)
        {
            _userService.UserIsExistOrExeption(id);
            return Ok(_projectService.GetUserStatistics(id));
        }
        [HttpGet("ProjectStatistics")]
        public IEnumerable<ProjectStatisticDTO> GetProjectStatistics()
        {
            return _projectService.GetProjectStatistics();
        }

    }
}
