﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using EF.Common.DTO.QueryResults;
using EF.Common.DTO.Team;
using EF.BLL.Interfaces;

namespace EF.WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService _teamService;
        public TeamsController(ITeamService teamService)
        {
            _teamService = teamService;
        }
        [HttpGet]
        public IEnumerable<TeamDTO> Get()
        {
            return _teamService.GetAll();
        }
        [HttpGet("{id}", Name = "TeamGet")]
        public ActionResult<TeamDTO> Get(int id)
        {
            return Ok(_teamService.Get(id));
        }
        [HttpPost]
        public ActionResult<TeamDTO> Post([FromBody] TeamCreateDTO team)
        {
            var newTeam = _teamService.Post(team);
            return CreatedAtRoute("TeamGet", new { newTeam.Id }, newTeam);
        }
        [HttpPut]
        public ActionResult<TeamDTO> Put([FromBody] TeamUpdateDTO team)
        {
            var updateTeam = _teamService.Update(team);
            return CreatedAtRoute("TeamGet", new { updateTeam.Id }, updateTeam);
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _teamService.Delete(id);
            return NoContent();
        }
        [HttpGet("TeamMembers")]
        public ActionResult<IEnumerable<TeamMembersDTO>> GetTeamMembers()
        {
            return Ok(_teamService.GetTeamMembers());
        }
    }
}
