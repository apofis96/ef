﻿using EF.Common.DTO.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
namespace EF.UI.Menus
{
    public class ProjectMenu
    {
        public static async Task Menu(HttpClient client, JsonSerializerOptions options)
        {
            string warning = "Sequence output is limited to three objects!";
            string pause = "Press any key to continue";
            string subMenuItems = @"1 : ReadAll
2 : Read(id)
3 : Create
4 : Update
5 : Delete
0 : Exit.";
            string subpath = "Projects";
            Console.WriteLine(subMenuItems);
            bool loop = true;
            while (loop)
            {
                Console.Write("Task menu input: ");
                char menu = Console.ReadKey().KeyChar;
                Console.WriteLine("\n================================");
                switch (menu)
                {
                    case '1':
                        Console.WriteLine(warning);
                        Console.WriteLine(pause);
                        Console.ReadKey();
                        Console.WriteLine(JsonSerializer.Serialize(JsonSerializer.Deserialize<IEnumerable<ProjectDTO>>(await client.GetStringAsync(subpath), options).Take(3), options));
                        break;
                    case '2':
                        Console.WriteLine(JsonSerializer.Serialize(JsonSerializer.Deserialize<ProjectDTO>(await client.GetStringAsync(subpath + "/" + MenuInputs.IdInput()), options), options));
                        break;
                    case '3':
                        var newProject = new ProjectCreateDTO();
                        Console.Write("Name: ");
                        newProject.Name = Console.ReadLine();
                        Console.Write("Description: ");
                        newProject.Description = Console.ReadLine();
                        Console.Write("Deadline: ");
                        newProject.Deadline = MenuInputs.DateTimeInput();
                        Console.Write("Author ");
                        newProject.AuthorId = MenuInputs.IdInput();
                        Console.Write("Team ");
                        newProject.TeamId = MenuInputs.IdInput();
                        HttpResponseMessage response = await client.PostAsync(subpath, new StringContent(JsonSerializer.Serialize(newProject), Encoding.UTF8, "application/json"));
                        if (response.StatusCode != HttpStatusCode.Created)
                        {
                            Console.WriteLine(response.StatusCode);
                            return;
                        }
                        Console.WriteLine(JsonSerializer.Serialize(JsonSerializer.Deserialize<ProjectDTO>(await response.Content.ReadAsStringAsync(), options), options));
                        Console.WriteLine("Done");
                        break;
                    case '4':
                        var updateProject = new ProjectUpdateDTO();
                        updateProject.Id = MenuInputs.IdInput();
                        Console.Write("Name: ");
                        updateProject.Name = Console.ReadLine();
                        Console.Write("Description: ");
                        updateProject.Description = Console.ReadLine();
                        Console.Write("Deadline: ");
                        updateProject.Deadline = MenuInputs.DateTimeInput();
                        Console.Write("Author ");
                        updateProject.AuthorId = MenuInputs.IdInput();
                        Console.Write("Team ");
                        updateProject.TeamId = MenuInputs.IdInput();
                        response = await client.PutAsync(subpath, new StringContent(JsonSerializer.Serialize(updateProject), Encoding.UTF8, "application/json"));
                        if (response.StatusCode != HttpStatusCode.Created)
                        {
                            Console.WriteLine(response.StatusCode);
                            return;
                        }
                        Console.WriteLine(JsonSerializer.Serialize(JsonSerializer.Deserialize<ProjectDTO>(await response.Content.ReadAsStringAsync(), options), options));
                        Console.WriteLine("Done");
                        break;
                    case '5':
                        response = await client.DeleteAsync(subpath + "/" + MenuInputs.IdInput());
                        if (response.StatusCode != HttpStatusCode.NoContent)
                        {
                            Console.WriteLine(response.StatusCode);
                            return;
                        }
                        Console.WriteLine("Done");
                        break;
                    case '0':
                        loop = false;
                        break;
                    default:
                        Console.WriteLine(subMenuItems);
                        break;
                }
            }
        }
    }
}
