﻿using EF.Common;
using EF.Common.DTO.ProjectTask;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace EF.UI.Menus
{
    public class TaskMenu
    {
        public static async Task Menu(HttpClient client, JsonSerializerOptions options)
        {
            string warning = "Sequence output is limited to three objects!";
            string pause = "Press any key to continue";
            string subMenuItems = @"1 : ReadAll
2 : Read(id)
3 : Create
4 : Update
5 : Delete
0 : Exit.";
            string subpath = "Tasks";
            Console.WriteLine(subMenuItems);
            bool loop = true;
            while (loop)
            {
                Console.Write("Task menu input: ");
                char menu = Console.ReadKey().KeyChar;
                Console.WriteLine("\n================================");
                switch (menu)
                {
                    case '1':
                        Console.WriteLine(warning);
                        Console.WriteLine(pause);
                        Console.ReadKey();
                        Console.WriteLine(JsonSerializer.Serialize(JsonSerializer.Deserialize<IEnumerable<ProjectTaskDTO>>(await client.GetStringAsync(subpath), options).Take(3), options));
                        break;
                    case '2':
                        Console.WriteLine(JsonSerializer.Serialize(JsonSerializer.Deserialize<ProjectTaskDTO>(await client.GetStringAsync(subpath + "/" + MenuInputs.IdInput()), options), options));
                        break;
                    case '3':
                        var newTask = new ProjectTaskCreateDTO();
                        Console.Write("Name: ");
                        newTask.Name = Console.ReadLine();
                        Console.Write("Description: ");
                        newTask.Description = Console.ReadLine();
                        Console.Write("FinishedAt: ");
                        newTask.FinishedAt = MenuInputs.DateTimeInput();
                        Console.Write("State ");
                        newTask.State = (TaskState)MenuInputs.IdInput();
                        Console.Write("ProjectId ");
                        newTask.ProjectId = MenuInputs.IdInput();
                        Console.Write("PerformerId ");
                        newTask.PerformerId = MenuInputs.IdInput();
                        HttpResponseMessage response = await client.PostAsync(subpath, new StringContent(JsonSerializer.Serialize(newTask), Encoding.UTF8, "application/json"));
                        if (response.StatusCode != HttpStatusCode.Created)
                        {
                            Console.WriteLine(response.StatusCode);
                            return;
                        }
                        Console.WriteLine(JsonSerializer.Serialize(JsonSerializer.Deserialize<ProjectTaskDTO>(await response.Content.ReadAsStringAsync(), options), options));
                        Console.WriteLine("Done");
                        break;
                    case '4':
                        var updateTask = new ProjectTaskUpdateDTO();
                        updateTask.Id = MenuInputs.IdInput();
                        Console.Write("Name: ");
                        updateTask.Name = Console.ReadLine();
                        Console.Write("Description: ");
                        updateTask.Description = Console.ReadLine();
                        Console.Write("FinishedAt: ");
                        updateTask.FinishedAt = MenuInputs.DateTimeInput();
                        Console.Write("State ");
                        updateTask.State = (TaskState)MenuInputs.IdInput();
                        Console.Write("ProjectId ");
                        updateTask.ProjectId = MenuInputs.IdInput();
                        Console.Write("PerformerId ");
                        updateTask.PerformerId = MenuInputs.IdInput();
                        response = await client.PutAsync(subpath, new StringContent(JsonSerializer.Serialize(updateTask), Encoding.UTF8, "application/json"));
                        if (response.StatusCode != HttpStatusCode.Created)
                        {
                            Console.WriteLine(response.StatusCode);
                            return;
                        }
                        Console.WriteLine(JsonSerializer.Serialize(JsonSerializer.Deserialize<ProjectTaskDTO>(await response.Content.ReadAsStringAsync(), options), options));
                        Console.WriteLine("Done");
                        break;
                    case '5':
                        response = await client.DeleteAsync(subpath + "/" + MenuInputs.IdInput());
                        if (response.StatusCode != HttpStatusCode.NoContent)
                        {
                            Console.WriteLine(response.StatusCode);
                            return;
                        }
                        Console.WriteLine("Done");
                        break;
                    case '0':
                        loop = false;
                        break;
                    default:
                        Console.WriteLine(subMenuItems);
                        break;
                }
            }
        }
    }
}
